<?php

/* _global/index.html */
class __TwigTemplate_bedb3abb515cb2879ccef209db1cf5d62a4ff8fd5e5bd0960d3dc4447edea1c3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'naslov' => array($this, 'block_naslov'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>

<head>
    <title>Hammer - ";
        // line 5
        $this->displayBlock('naslov', $context, $blocks);
        echo "</title>
    <meta charset=\"UTF-8\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/css/main.css\" />
</head>

<body>
    <header class=\"site-header\">
        <div class=\"banners\">
            <a href=\"/\" class=\"banner\">
                <img src=\"/assets/img/banner-1.jpg\" alt=\"Banner 1\">
            </a>
        </div>
        <div class=\"social-icons\">
            <a href=\"#\"><img src=\"/assets/img/social/linkedin.png\" alt=\"Linkedin\"></a>
            <a href=\"#\"><img src=\"/assets/img/social/facebook.png\" alt=\"Facebook\"></a>
            <a href=\"#\"><img src=\"/assets/img/social/twitter.png\" alt=\"Twitter\"></a>
            <a href=\"#\"><img src=\"/assets/img/social/google-plus.png\" alt=\"Google-plus\"></a>
            <a href=\"#\"><img src=\"/assets/img/social/youtube.png\" alt=\"Youtube\"></a>
        </div>
        <div class=\"search-box\">
            <form method=\"POST\" action=\"/search\">
                <input type=\"text\" name=\"q\" placeholder=\"Kljucne reci pretrage\">
                <button type=\"submit\">Search</button>
            </form>
        </div>
        <nav id=\"main-menu\">
            <ul>
                <li><a href=\"/\">Pocetna</a>
                <li><a href=\"/categories\">Kategorije</a>
                <li><a href=\"/profile\">Profil</a>
                <li><a href=\"/contact\">Kontakt</a>
                <li><a href=\"/log-out\">Odjava</a>
            </ul>
        </nav>
    </header>
    <main>
        ";
        // line 41
        $this->displayBlock('main', $context, $blocks);
        // line 43
        echo "    </main>
    <footer class=\"site-footer\">
        &copy; Hammer Auction House 2018
    </footer>
</body>

</html>";
    }

    // line 5
    public function block_naslov($context, array $blocks = array())
    {
        echo "Pocetna";
    }

    // line 41
    public function block_main($context, array $blocks = array())
    {
        // line 42
        echo "        ";
    }

    public function getTemplateName()
    {
        return "_global/index.html";
    }

    public function getDebugInfo()
    {
        return array (  91 => 42,  88 => 41,  82 => 5,  72 => 43,  70 => 41,  31 => 5,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "_global/index.html", "/opt/lampp/htdocs/views/_global/index.html");
    }
}
