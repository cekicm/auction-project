<?php
namespace App\Controllers;

class MainController extends \App\Core\Controller
{
    public function home()
    {
        $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
        $categories = $categoryModel->getAll();
        $this->set('categories', $categories);

        $staraVrednost = $this->getSession()->get('brojac', 0);
        $novaVrednost = $staraVrednost + 1;
        $this->getSession()->put('brojac', $novaVrednost);
        
        $this->set('podatak', $novaVrednost);
    }
}
